#!/bin/sh

install_pkg () {
	pkg="$1"
	installed=$(pacman -Q "$pkg" 2>/dev/null | grep -c "$pkg")
	if [ "$installed" = 0 ]; then
		sudo pacman --needed --noconfirm -S "$pkg"
	fi
}

install_pkgs () {
	pkg_list="/tmp/cura_pkgs.txt"
	while read -r line
	do
		install_pkg "$line"
	done < $pkg_list
}

check_install () {
	pkg="$1"
	pkg_available=$(pacman -Ss ^python-"$pkg"$ | grep -c "$pkg")
	if [ "$pkg_available" = 1 ]; then
		echo "python-$pkg" >> /tmp/cura_pkgs.txt
	else
		echo "$pkg" >> /tmp/cura_modules.txt
	fi

}

check_deps () {
	check_and_install_pkg wget
	check_and_install_pkg python-pip
}

download_python_module_list () {
	# download the module list from Ultimaker repo from github into /tmp
	wget -O /tmp/cura_requirements.txt https://raw.githubusercontent.com/Ultimaker/Cura/master/requirements.txt
}

install_mods () {
	mod_list="/tmp/cura_modules.txt"
	while read -r line
	do
	mod_installed=$(pip list | grep -c "$line")
	if [ "$mod_installed" -eq 0 ] && ! [ "$line" = "pywin32" ] ; then
	sudo pip install "$line"
	fi
	done < $mod_list
}

parse_list () {
	input="/tmp/cura_requirements.txt"
	while read -r line
	do
		pkg=$(echo "$line" | cut -d "=" -f 1)
		check_install "$pkg"
	done < "$input"
}

install_cura () {
	install_pkg cura
	install_pkg cura-binary-data
	install_pkg python-libcharon

}

cleanup () {
	rm -r /tmp/cura*
}

main () {
	check_deps
	download_python_module_list
	parse_list
	install_pkgs
	install_mods
	install_cura
	cleanup
}

#
main

