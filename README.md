
#What is this?

In ARCH (and deriviates like Manjaro, EndeavourOS, etc.) Linux there are two ways to get the Ultimaker Cura 3D slicer installed. 
1. The appimage from the Ultimaker website.
2. The cura package from the repos.

If you choose the cura package the Cura engine will be installed but several dependencies of the plugins that ship with Cura will not be.  This will create an unstable usage of Cura and several plugins wont work, and a few may even bring up an alert when cura starts. Not to mention that Ultimaker can change what plugins ship by default with each version of Cura, so it wouldnt hurt to have a script that can figure all this out for you.  So, I wrote it.

#How does it work?

The official Ultimaker git repo includes a file listing all the python dependencies required for the current version.  This script will download the file, then process each item in the list and install the python modules.  It will check each dependency of cura and determine if it can be installed as a python package from the repos, or a python module from pip.  Cura 4.13.1 depends on 11 python modules that are not packages that can be installed with pacman.

#Issues

This script is not as picky about python module versions as perhaps it should be.  As pacman packages are updated it is likely that your Arch based system will get ahead of the Cura dependencies.  This is generally not an problem as most incremental updates are bacwards compatible, but there remains the possibility that an updated python package supplied by the Arch repos will introduce a breaking change.

#Future Plans

It might be nice to have an option to install all the packages with pip for the python purists out there.  And it might also be helpful if I knew if the same issue existed on other distros and port this to them if required.
